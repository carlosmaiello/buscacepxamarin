﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BuscaCEP.DAO;
using BuscaCEP.Modelo;

namespace BuscaCEPTest
{
    [TestClass]
    public class CepTest
    {
        [TestMethod]
        public void TestConsultar()
        {
            Endereco endereco = EnderecoDAO.Instance.consultar("17014505");
            Assert.AreEqual<string>("Bauru", endereco.Localidade);
        }
    }
}
