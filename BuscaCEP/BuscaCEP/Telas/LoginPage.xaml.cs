﻿using BuscaCEP.DAO;
using BuscaCEP.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BuscaCEP.Telas
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LoginPage : ContentPage
	{
        public string Email { get; set; }
        public string Senha { get; set; }

        public LoginPage ()
		{
			InitializeComponent ();
            Email = "carlos.mjunior@sp.senac.br";
            Senha = "123";
            BindingContext = this;
		}

        private void Button_Clicked(object sender, EventArgs e)
        {
            try
            {
                Usuario usuario = UsuarioDAO.Instance.consultar(this.Email, this.Senha);

                App.Current.MainPage = new NavigationPage(new EnderecoPage());

                //DisplayAlert("Usuario Logado", usuario.Nome, "Fechar");
            }
            catch (Exception ex)
            {
                DisplayAlert("Erro", ex.Message, "Fechar");
            }
        }
    }
}