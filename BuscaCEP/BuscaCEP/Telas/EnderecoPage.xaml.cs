﻿using BuscaCEP.DAO;
using BuscaCEP.Modelo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BuscaCEP.Telas
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EnderecoPage : ContentPage, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public string EnderecoText { get; set; }
        public string Cep { get; set; }
        public EnderecoPage()
        {
            InitializeComponent();
            BindingContext = this;
        }

        public void BuscarCep_Clicked(object sender, EventArgs e)
        {

            Endereco endereco = EnderecoDAO.Instance.consultar(this.Cep);

            EnderecoText = endereco.Description;

            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("EnderecoText"));
        }

        public void VerHistorico_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new EnderecoHistoricoPage());

        }
    }
}