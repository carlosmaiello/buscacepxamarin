﻿using BuscaCEP.DAO;
using BuscaCEP.Modelo;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BuscaCEP.Telas
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class EnderecoHistoricoPage : ContentPage, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public ObservableCollection<Endereco> Enderecos;

		public EnderecoHistoricoPage ()
		{
			InitializeComponent ();
            BindingContext = new ObservableCollection<Endereco>(EnderecoDAO.Instance.consultarTodos());

            //Enderecos = new ObservableCollection<Endereco>(EnderecoDAO.Instance.consultarTodos());
            //PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Enderecos"));
        }
    }
}