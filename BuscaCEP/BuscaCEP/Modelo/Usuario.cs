﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace BuscaCEP.Modelo
{
    public class Usuario
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public String Nome { get; set; }
        [Unique]
        public String Email { get; set; }
        public String Senha { get; set; }


    }
}
