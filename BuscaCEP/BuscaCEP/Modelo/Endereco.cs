﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace BuscaCEP.Modelo
{
    public class Endereco
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public string Cep { get; set; }
        public string Logradouro { get; set; }
        public string Complemento { get; set; }
        public string Bairro { get; set; }
        public string Localidade { get; set; }
        public string Uf { get; set; }
        public string Unidade { get; set; }
        public string Ibge { get; set; }
        public string Gia { get; set; }

        [Ignore]
        public string Description
        {
            get
            {
                return Cep + "\n" + Logradouro + "\n" + Bairro + "\n" + Localidade + "/" + Uf;
            }
        }

    }
}
