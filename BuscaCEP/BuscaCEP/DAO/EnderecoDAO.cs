﻿using BuscaCEP.Modelo;
using Newtonsoft.Json;
using SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace BuscaCEP.DAO
{

    /**
     *  https://viacep.com.br/ws/01001000/json/  
     */
    public class EnderecoDAO
    {
        private static EnderecoDAO instance;
        public static EnderecoDAO Instance {
            get {
                if (instance == null)
                {
                    instance = new EnderecoDAO();
                }

                return instance;
            }
            private set {
                instance = value;
            }
        }

        
        private EnderecoDAO()
        {
            
        }

        public Endereco consultar(string cep)
        {
            string url = "http://viacep.com.br/ws/" + cep + "/json/";
            string json = new WebClient().DownloadString(url);
            var e = JsonConvert.DeserializeObject<Endereco>(json);
            Db.Instance.Conn().Insert(e);
            return e;
        }

        public List<Endereco> consultarTodos()
        {
            return (from e in Db.Instance.Conn().Table<Endereco>()
                   orderby e.Id descending
                   select e).ToList();
        }



    }
}
