﻿using BuscaCEP.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BuscaCEP.DAO
{
    public class UsuarioDAO
    {
        private static UsuarioDAO instance;
        public static UsuarioDAO Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new UsuarioDAO();
                }

                return instance;
            }
            private set
            {
                instance = value;
            }
        }


        private UsuarioDAO()
        {
        }
        public void inserir(Usuario usuario)
        {
            if (usuario.Nome == "")
            {
                throw new Exception("Nome inválido");
            }

            if (usuario.Email == "")
            {
                throw new Exception("E-mail inválido");
            }

            if (usuario.Senha == "")
            {
                throw new Exception("Senha inválida");
            }

            Db.Instance.Conn().Insert(usuario);
        }

        public void alterar(Usuario usuario)
        {
            if (usuario.Id <= 0)
            {
                throw new Exception("Usuário inválido");
            }

            if (usuario.Nome == "")
            {
                throw new Exception("Nome inválido");
            }

            if (usuario.Email == "")
            {
                throw new Exception("E-mail inválido");
            }

            if (usuario.Senha == "")
            {
                throw new Exception("Senha inválida");
            }

            Db.Instance.Conn().Update(usuario);
        }

        public void remover(Usuario usuario)
        {
            if (usuario.Id <= 0)
            {
                throw new Exception("Usuário inválido");
            }

            Db.Instance.Conn().Delete(usuario);
        }

        public Usuario consultar(int id)
        {
            return Db.Instance.Conn().Get<Usuario>(id);
        }

        public List<Usuario> consultarTodos()
        {
            return Db.Instance.Conn().Table<Usuario>().ToList();
        }

        public Usuario consultar(String email, String senha)
        {
            if (email == "")
            {
                throw new Exception("E-mail inválido");
            }

            if (senha == "")
            {
                throw new Exception("Senha inválida");
            }

            return (from u in Db.Instance.Conn().Table<Usuario>()
                    where u.Email == email && u.Senha == senha
                select u).First();
        }
    }
}
