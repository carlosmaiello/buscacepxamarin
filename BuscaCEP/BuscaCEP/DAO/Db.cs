﻿using BuscaCEP.Modelo;
using SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace BuscaCEP.DAO
{
    class Db
    {
        private static Db instance;
        public static Db Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Db();
                }

                return instance;
            }
            private set
            {
                instance = value;
            }
        }

        private SQLiteConnection conn;

        private Db()
        {

        }

        public SQLiteConnection Conn()
        {
            if (conn == null)
            {
                string dbPath = Path.Combine(
                Environment.GetFolderPath(Environment.SpecialFolder.Personal),
                "buscacep.db3");
                conn = new SQLiteConnection(dbPath);
                conn.CreateTable<Endereco>();
                conn.CreateTable<Usuario>();
            }

            return conn;
        }
    }

}
