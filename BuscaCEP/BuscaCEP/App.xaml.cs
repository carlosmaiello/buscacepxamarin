using BuscaCEP.DAO;
using BuscaCEP.Telas;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation (XamlCompilationOptions.Compile)]
namespace BuscaCEP
{
	public partial class App : Application
	{
		public App ()
		{
			InitializeComponent();

            //UsuarioDAO.Instance.inserir(new Modelo.Usuario
            //{
            //    Nome = "Carlos Maiello",
            //    Email = "carlos.mjunior@sp.senac.br",
            //    Senha = "123"
            //});

            //UsuarioDAO.Instance.inserir(new Modelo.Usuario
            //{
            //    Nome = "Jos� Silva",
            //    Email = "jose@silva.com.br",
            //    Senha = "123"
            //});

            MainPage = new LoginPage();
		}

		protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}
